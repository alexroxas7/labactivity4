package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;
    
    //Constructor
    public Vector3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Accessors
    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }

    //Methods
    public double magnitude()
    {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }   

    public double dotProduct(Vector3d second)
    {
        return ((this.x * second.getX()) + (this.y * second.getY()) + (this.z * second.getZ())); 
    }

    public Vector3d add(Vector3d second)
    {
        double newX = this.x + second.getX();
        double newY = this.y + second.getY();
        double newZ = this.z + second.getZ();
        
        Vector3d result = new Vector3d(newX, newY, newZ);
        return result;
    }

    //Main Method
    public static void main(String args[]){
        Vector3d original = new Vector3d(1, 1, 2);
        System.out.println(original.getX());
        System.out.println(original.getY());
        System.out.println(original.getZ());

        System.out.println(original.magnitude());
        System.out.println(original.dotProduct(new Vector3d(2, 3, 4)));
        Vector3d resultV = original.add(new Vector3d(2, 3, 4));
        System.out.println(resultV.getX());
        System.out.println(resultV.getY());
        System.out.println(resultV.getZ());
    }
}
