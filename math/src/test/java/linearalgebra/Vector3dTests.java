//Alexander Roxas
//2136838
package linearalgebra;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Vector3dTests {
        /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testConstructor()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        assertEquals(1.0, testVector.getX(), 0);
        assertEquals(2.0, testVector.getY(), 0);
        assertEquals(3.0, testVector.getZ(), 0);
    }

    @Test
    public void testMagnitude()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        assertEquals(3.7416, testVector.magnitude(), 4);
    }

    @Test
    public void testDot()
    {
        Vector3d testVector = new Vector3d(1,1,2);
        Vector3d inputVector = new Vector3d(2,3,4);
        assertEquals(13.0, testVector.dotProduct(inputVector), 0);
    }

    @Test
    public void testAdd()
    {
        Vector3d testVector = new Vector3d(1, 1, 2);
        Vector3d inputVector = new Vector3d(2, 3, 4);
        Vector3d resultVector = testVector.add(inputVector);
        assertEquals(3, resultVector.getX(), 0);
        assertEquals(4, resultVector.getY(), 0);
        assertEquals(6, resultVector.getZ(), 0);
    }
}
